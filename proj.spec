Name:             proj
Version:          9.5.1
Release:          1
Summary:          A generic coordinate transformation software
License:          MIT
URL:              https://proj.org
Source0:          https://download.osgeo.org/proj/proj-%{version}.tar.gz
Source1:          https://download.osgeo.org/proj/proj-data-1.20.tar.gz
BuildRequires:    cmake >= 3.16
BuildRequires:    make
BuildRequires:    gcc-c++
BuildRequires:    sqlite-devel >= 3.11
BuildRequires:    libtiff-devel
BuildRequires:    curl-devel
BuildRequires:    gtest-devel >= 1.8.0
Requires:         proj-data = %{version}-%{release}
Obsoletes:        proj-nad < 9.5.1-1

%description
PROJ is a generic coordinate transformation software that transforms
geospatial coordinates from one coordinate reference system (CRS) to
another. This includes cartographic projections as well as geodetic
transformations. PROJ is released under the X/MIT open source license.

%package_help

%package          devel
Summary:          Development files for PROJ.4
Requires:         proj = %{version}-%{release}
Provides:         proj-static = %{version}-%{release}
Obsoletes:        proj-static < %{version}-%{release}

%description      devel
This package contains libproj and the appropriate header files and man pages
and libproj static library.This package alse contains additional US and
Canadian datum shift grids and additional EPSG dataset.

%package data
Summary:        Proj data files
BuildArch:      noarch
 
%description data
Proj arch independent data files.

# TODO: why the \ cruft in this section?
%define data_subpkg(c:n:e:s:) \
%define countrycode %{-c:%{-c*}}%{!-c:%{error:Country code not defined}} \
%define countryname %{-n:%{-n*}}%{!-n:%{error:Country name not defined}} \
%define extrafile %{-e:%{_datadir}/%{name}/%{-e*}} \
%define wildcard %{!-s:%{_datadir}/%{name}/%{countrycode}_*} \
\
%package data-%{countrycode}\
Summary:      %{countryname} datum grids for Proj\
BuildArch:    noarch\
# See README.DATA \
License:      CC-BY-4.0 OR CC-BY-SA-4.0 OR MIT OR BSD-2-Clause OR CC0-1.0\
Requires:     proj-data = %{version}-%{release} \
Supplements:  proj\
\
%description data-%{countrycode}\
%{countryname} datum grids for Proj.\
\
%files data-%{countrycode}\
%{wildcard}\
%{extrafile}


%data_subpkg -c ar -n Argentina
%data_subpkg -c at -n Austria
%data_subpkg -c au -n Australia
%data_subpkg -c be -n Belgium
%data_subpkg -c br -n Brasil
%data_subpkg -c ca -n Canada
%data_subpkg -c ch -n Switzerland -e CH
%data_subpkg -c cz -n Czech
%data_subpkg -c de -n Germany
%data_subpkg -c dk -n Denmark -e DK
%data_subpkg -c es -n Spain
%data_subpkg -c eur -n %{quote:Nordic + Baltic} -e NKG
%data_subpkg -c fi -n Finland
%data_subpkg -c fo -n %{quote:Faroe Island} -e FO -s 1
%data_subpkg -c fr -n France
%data_subpkg -c hu -n Hungary
%data_subpkg -c is -n Island -e ISL
%data_subpkg -c jp -n Japan
%data_subpkg -c mx -n Mexico
%data_subpkg -c no -n Norway
%data_subpkg -c nc -n %{quote:New Caledonia}
%data_subpkg -c nl -n Netherlands
%data_subpkg -c nz -n %{quote:New Zealand}
%data_subpkg -c pl -n Poland
%data_subpkg -c pt -n Portugal
%data_subpkg -c se -n Sweden
%data_subpkg -c sk -n Slovakia
%data_subpkg -c si -n Slovenia
%data_subpkg -c uk -n %{quote:United Kingdom}
%data_subpkg -c us -n %{quote:United States}
%data_subpkg -c za -n %{quote:South Africa}

%prep
%autosetup -n proj-%{version} -p1

%build
%cmake -DUSE_EXTERNAL_GTEST=ON -DTESTING_USE_NETWORK=OFF
%cmake_build

%install
%cmake_install
rm -fr %{buildroot}%{_datadir}/doc/%{name}

# Install data
mkdir -p %{buildroot}%{_datadir}/%{name}
tar -xf %{SOURCE1} --directory %{buildroot}%{_datadir}/%{name}

%check
%ctest

%files
%license COPYING
%doc AUTHORS.md NEWS.md
%{_bindir}/cct
%{_bindir}/cs2cs
%{_bindir}/geod
%{_bindir}/gie
%{_bindir}/invgeod
%{_bindir}/invproj
%{_bindir}/proj
%{_bindir}/projinfo
%{_bindir}/projsync
%{_libdir}/libproj.so.25*

%files help
%doc NEWS.md README.md ChangeLog
%{_mandir}/man1/*.1*

%files devel
%{_includedir}/*.h
%{_includedir}/proj
%{_libdir}/libproj.so
%{_libdir}/cmake/proj
%{_libdir}/cmake/proj4
%{_libdir}/pkgconfig/proj.pc

%files data
%doc README.md
%dir %{_datadir}/%{name}
%{_datadir}/%{name}/CH
%{_datadir}/%{name}/GL27
%{_datadir}/%{name}/ITRF2000
%{_datadir}/%{name}/ITRF2008
%{_datadir}/%{name}/ITRF2014
%{_datadir}/%{name}/ITRF2020
%{_datadir}/%{name}/nad.lst
%{_datadir}/%{name}/nad27
%{_datadir}/%{name}/nad83
%{_datadir}/%{name}/other.extra
%{_datadir}/%{name}/proj.db
%{_datadir}/%{name}/proj.ini
%{_datadir}/%{name}/world
%{_datadir}/%{name}/README.DATA
%{_datadir}/%{name}/copyright_and_licenses.csv
%{_datadir}/%{name}/deformation_model.schema.json
%{_datadir}/%{name}/projjson.schema.json
%{_datadir}/%{name}/triangulation.schema.json

%changelog
* Mon Dec 16 2024 Funda Wang <fundawang@yeah.net> - 9.5.1-1
- update to 9.5.1

* Tue Nov 19 2024 Funda Wang <fundawang@yeah.net> - 9.4.1-2
- adopt to new cmake macro

* Thu Jun 20 2024 xu_ping <707078654@qq.com> - 9.4.1-1
- Upgrade to 9.4.1

* Thu Oct 19 2023 yaoxin <yao_xin001@hoperun.com> - 9.3.0-1
- Upgrade to 9.3.0

* Wed Dec 14 2022 caodongxia <caodongxia@h-partners.com> - 8.2.1-2
- Add the compilation dependency make

* Tue Jan 18 2022 yaoxin <yaoxin30@huawei.com> - 8.2.1-1
- Upgrade proj to 8.2.1

* Wed Feb 19 2020 Senlin Xia <xiasenlin1@huawei.com> 4.9.3-8
- package init
